package com.example.kokaikai;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class register extends AppCompatActivity {

    private EditText MyEmail, MyPassword;
    private Button  mRegisBtn;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener fStateListener;
    private static final String TAG = register.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register3);
        MyEmail = findViewById(R.id.editName);
        MyPassword = findViewById(R.id.editPassword);
        progressBar = findViewById(R.id.progressBar2);
        mAuth = FirebaseAuth.getInstance();
        mRegisBtn = findViewById(R.id.btn_enter);
        mAuth = FirebaseAuth.getInstance();
//        fStateListener = FirebaseAuth.AuthStateListener() {
//            @Override
//            public void
//        }
        fStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User sedang login
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User sedang logout
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        mRegisBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerNewUser(MyEmail.getText().toString(), MyPassword.getText().toString());
            }
        });
        mRegisBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerNewUser(MyEmail.getText().toString(), MyPassword.getText().toString());
            }
        });
    }
    private void registerNewUser(final String MyEmail, String MyPassword) {
        progressBar.setVisibility(View.VISIBLE);
        final String email, password;
        email = MyEmail;
        password = MyPassword;

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Please enter email...", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Please enter password!", Toast.LENGTH_LONG).show();
            return;
        }
        mAuth.createUserWithEmailAndPassword(MyEmail, MyPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            task.getException().printStackTrace();
                            Toast.makeText(register.this, "Registration Successfull!" + "email " + email, Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(register.this, Main.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(register.this,  "Registration Failed", Toast.LENGTH_LONG).show();

                        }
                    }
                });
    }
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(fStateListener);
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (fStateListener != null) {
            mAuth.removeAuthStateListener(fStateListener);
        }
    }
}