package com.example.kokaikai;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class home extends AppCompatActivity {

    private ImageButton button3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        BottomNavigationView navigationView = (BottomNavigationView) findViewById(R.id.btm_nav);


        button3 = findViewById(R.id.btn_home);

//         DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_id);

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent button3 = new Intent(getApplicationContext(), menu.class);
                startActivity(button3);
            }
        });

         navigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();

                switch (id) {
                    case R.id.sign_out:
                        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
                        firebaseAuth.signOut();
                        Intent intent = new Intent(home.this, Main.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;

                }


//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_id);
//                drawer.closeDrawer(GravityCompat.START);
//                int id = menuItem.getItemId();
////
////                if (id == R.id.home) {
////
////                    HomeFragment fragment = new HomeFragment();
////                    FragmentTransaction fragmentTransaction = getSupportFragmentManager() .beginTransaction();
////                    fragmentTransaction.replace(R.id.frame_layout, fragment);
////                    fragmentTransaction.commit();
////
////                }
////
////                if (id == R.id.favorite) {
////
////                    FavoriteFragment fragment = new FavoriteFragment();
////                    FragmentTransaction fragmentTransaction = getSupportFragmentManager() .beginTransaction();
////                    fragmentTransaction.replace(R.id.frame_layout, fragment);
////                    fragmentTransaction.commit();
////
////                }
////
////                if (id == R.id.profile) {
////
////                    ProfileFragment fragment = new ProfileFragment();
////                    FragmentTransaction fragmentTransaction = getSupportFragmentManager() .beginTransaction();
////                    fragmentTransaction.replace(R.id.frame_layout, fragment);
////                    fragmentTransaction.commit();
////
////                }

            }
        });


        navigationView.setSelectedItemId(R.id.home);
    }

}
