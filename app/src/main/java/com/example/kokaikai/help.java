package com.example.kokaikai;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class help extends AppCompatActivity {

    private Button enter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        enter = (Button) findViewById(R.id.t_late);

        enter.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent enter = new Intent(getApplicationContext(),Main.class);
                startActivity(enter);
            }
        }));


    }
}