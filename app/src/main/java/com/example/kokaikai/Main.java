package com.example.kokaikai;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Main extends AppCompatActivity {
    EditText MyEmail,MyPassword;
    Button mLoginBtn;
    TextView mCreateBtn;
    ProgressBar progressBar;
    FirebaseAuth mAuth;
    FirebaseUser mUser;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyEmail = findViewById(R.id.editUser);
        MyPassword = findViewById(R.id.edtPass);
        progressBar = findViewById(R.id.progressBar1);
        mAuth = FirebaseAuth.getInstance();
        mLoginBtn = findViewById(R.id.btn_enter);

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = MyEmail.getText().toString().trim();
                String password = MyPassword.getText().toString().trim();

                if(TextUtils.isEmpty(email)){
                    MyEmail.setError("Email is Required.");
                    return;
                }

                if(TextUtils.isEmpty(password)){
                    MyPassword.setError("Password is Required.");
                    return;
                }

                if(password.length() < 8){
                    MyPassword.setError("Password Must be >= 8 Characters");
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                // authenticate the user



                mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(Main.this, "Logged in Successfully", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
//
                            Intent intent = new Intent(Main.this, home.class);
                            startActivity(intent);
                        }else {
                            Toast.makeText(Main.this, "Error ! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        }
//

                    }
                });

            }
        });

    }

}

